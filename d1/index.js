/*
	MongoDB - Query Operators
	Expand Querries in MongoDB using Query Operators
*/


/*
	What does Query Operator mean?
	-A query is a request for data from a database.
	-An operator is a symbol that represents an action or a process
	-Putting them together, they mean the things that we can do on our queries using certain operators.
*/

/*
	Why do we need to study query operators?
	- knowing it will enable us to create queries that we can do more than what simple operators do. (We can do more than what we do in simple CRUD Operations)

	- Example: in our CRUD Operations discussion, we have discussed findOne using a specific value inside its single or multiple parameters. When we know query operators, we may look for records that are more specific.
*/

//1. COMPARISON QUERY OPERATORS
/*
	Includes:
		-greater than
		-less than
		greater than or equal to
		-less than or equal to
		-not equal to
		-in
*/

/*
	1.1 GREATER THAN: "$gt"
		- finds documents that have fields numbers that are greater than a specified value.

		Syntax:
			db.collectionName.find({field: {$gt: value}});
*/

db.users.find({age: {gt:76}}); 

/*
	1.2 GREATER THAN OR EQUAL TO: "$gte"
	- finds documents that have field number values that are greater than or equal to a specific value.

	Syntax:
		db.collectionName.find({field: {$gte: value}});
*/

db.users.find({age: {$gte: 76}});

/*
	1.3 LESS THAN OPERATOR: "$lt"
	- finds documents that have field number values less than the specified value.

	Syntax:
		db.collectionName.find({field: {$lt: value}});
*/
db.users.find({age: {$lt: 65}});

/*
	1.4 LESS THAN OR EQUAL TO OPERATOR: "$lte"
	- finds documents that have field number that are less than or equal to a specified value.

	Syntax:
		db.collectionName.find({field: { $lte: value}});
*/

db.users.find({age: {$lte: 65}});


/*
	1.5 NOT EQUAL TO OPERATOR: "$ne"
	- finds documents that have field number that are not equal to a specified vlaue

	Syntax:
		db.collectionName.find({field: { $ne: value}});
*/

db.users.find({age: {$ne: 65}});

/*
	1.6 INOPERATOR: "$in"
	- finds documents with specific match criteria on one field using different values.

	Syntax:
		db.collectionName.find({field: { $in: value}});
*/

db.users.find({lastName: {$in: ["Hawking", "Doe"]}});
db.users.find({courses: {$in: ["HTML", "React"]}});



/*
	SOURCES:

	MongoDB Greater Than Operator:	https://docs.mongodb.com/manual/reference/operator/query/gt/
	MongoDB Greater Than Or Equal To Operator	https://docs.mongodb.com/manual/reference/operator/query/gte/
	MongoDB Less Than Operator:
	https://docs.mongodb.com/manual/reference/operator/query/lt/
	MongoDB Less Than Or Equal To Operator:
	https://docs.mongodb.com/manual/reference/operator/query/lte/
	MongoDB Not Equal Operator:
	https://docs.mongodb.com/manual/reference/operator/query/ne/
	MongoDB In Operator:
	https://docs.mongodb.com/manual/reference/operator/query/in/
*/


//2. EVALUATION QUERY OPERATORS
//They return data based on evaluations of either individual fields or the entire collections documents.

/*
	2.1 REGEX OPERATOR: "$regex"
	- short for "regular expression"
	- they are called regular expression because they are based on regular languages.
	- used for matching strings.
	- it allows us to find documents that match a specific string pattern using regular expressions.
*/

/*
	2.1.1 Case Sensitive Query
	- Syntax:
		db.collectionName.find({field: {$regex: 'pattern'}});
*/

db.users.find({lastName: {$regex: 'A'}});

/*
	2.1.2 Case Insensitive Query
	- we can run case-insensitive query by utilizing the "i" option.

	- Syntax:
		db.collectionName.find({field: {$regex: 'pattern', $options: '$optionValue'}});
*/

db.users.find({lastName: {$regex: 'A', $options: '$i'}});


//MINI-ACTIVITY #1

db.users.find({firstName: {$regex: 'e', $options: '$i'}});

//3. LOGICAL QUERY OPERATORS

/*
	3.1 OR OPERATOR: "$or"
	- finds documents that match a single criteria from multiple provided search criteria.

	Syntax:
		db.collectionName.find({$or: [{fieldA: valueA}, {fieldB:valueB}]});
*/

db.users.find({$or: [{firstName: "Neil"}, {age: "25"}]});
db.users.find({$or: [{firstName: "Neil"}, {age: {$gt:30}}]});

/*
	3.1 OR OPERATOR: "$and"
	- finds documents that matching multiple criteria in a single field.

	Syntax:
		db.collectionName.find({$and: [{fieldA: valueA}, {fieldB:valueB}]});
*/

db.users.find({$and: [{age: {$ne: 82}}, {age: {$ne: 76}}]});

//MINI-ACTIVITY #2

db.users.find({$and: [{firstName: {$regex: 'e', $options: '$i'}}, {age: {$lte: 30}}]});

//FIELD PROJECTION
	//By default return the whole document especially when dealing with complex documents but sometimes, it is not helpful to view the whole document, especially when dealing with complex documents.
	//To help with readability of the values returned(or sometimes, because of security reasons), we include or exclude some fields.
	//In other words, we project our selected fields.

/*
	1. INCLUSION
		- allows us to include/add specific field only when retriveing documents.
		- the value denoted is "1" to indicate that the field is being included.
		- we cannot do exclusion on field that uses inclusion projection.

		Syntax:
			db.collectionName.find({criteria}, {field:1});
*/

db.users.find({firstName: "Jane"});

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
	}
);

/*
	1.1 RETURNING SPECIFIC IN EMBEDDED DOCUMENTS
		- double quotations are important.
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 1
	}
);

/*
	1.2 EXCEPTION TO THE INCLUSION RULE: Suppressing the ID field.
		- allows us to exclude the "_id" field when retrieving documents.
		- when using field projection, field inclusion and exclusion may not be used at the same time.
		- excluding the "_id" field is the ONLY exception to this rule.

		Syntax:
			db.collectionName.find({criteria}, {id:0});
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		firstName: 1,
		lastName: 1,
		contact: 1,
		_id: 0
	}
);

/*
	1.3 SLICE OPERATOR: "$slice"
		- allows us to retrieve only one element that matches the search criteria.
*/

db.users.insert({
	nameArr: [
		{
			nameA: "Juan"
		},
		{
			nameB: "Tamad"
		}
	]
});

db.users.find({
	nameArr:
	{
		nameA: "Juan"
	}
});

//SLICE OPERATOR

db.users.find(
			{ "namearr": 
				{ 
					namea: "Juan" 
				} 
			}, 
			{ namearr: 
				{ $slice: 1 } 
			}
		)


//MINI_ACTIVITY #3

db.users.find(
	{
		firstName: {$regex: 's', $options: '$i'}, 
	},
	{
		firstName: 1,
		lastName: 1,
		_id: 0
	}
	);


/*
	2. EXCLUSION
		- allows us to exclude or remove specific fields when retrieving documents
		- the value provided is zero to denote that the field is being included.

		Syntax:
			db.collectionName.find({criteria}, field: 0);
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		contact: 0,
		department: 0
	}
);

/*
	2.1 EXCLUDING/SURPRESSING SPECIFIC FIELDS IN EMBEDDED DOCUMENT
*/

db.users.find(
	{
		firstName: "Jane"
	},
	{
		"contact.phone": 0
	}
);